object ca2_worksheet {
	
	//statements:
	val aList:List[Int] = List(1, 2, 3)       //> aList  : List[Int] = List(1, 2, 3)
	val bList=List("edom", "odsoft", "tap")   //> bList  : List[String] = List(edom, odsoft, tap)
	val cList=List('a', 'b')                  //> cList  : List[Char] = List(a, b)
	val dList=List(true, false)               //> dList  : List[Boolean] = List(true, false)
	val e=5.6                                 //> e  : Double = 5.6
	val fList = List(1.0, 2, 3)               //> fList  : List[Double] = List(1.0, 2.0, 3.0)
	val g='i'                                 //> g  : Char = i
	
	
	
	
	
	/*------------- EXERCISE 1 -------------*/
	val aRes = aList ::: 0.1 :: Nil           //> aRes  : List[AnyVal{def getClass(): Class[_ >: Double with Int <: AnyVal]}] 
                                                  //| = List(1, 2, 3, 0.1)
	val bRes = cList ::: 0.2 :: Nil           //> bRes  : List[AnyVal{def getClass(): Class[_ >: Double with Char <: AnyVal]}]
                                                  //|  = List(a, b, 0.2)
	val cRes = e :: 5 :: Nil                  //> cRes  : List[AnyVal] = List(5.6, 5)
	val dRes = e :: 5.1 :: Nil                //> dRes  : List[Double] = List(5.6, 5.1)
	val eRes = aList ::: "4" :: Nil           //> eRes  : List[Any] = List(1, 2, 3, 4)
	val fRes = fList ::: "4" :: Nil           //> fRes  : List[Any] = List(1.0, 2.0, 3.0, 4)
	val gRes = g :: "j" :: Nil                //> gRes  : List[Any] = List(i, j)
	
	
	
	
	
	/*------------- EXERCISE 2 -------------*/
	val mandatoryList = List("TAP", "EDOM", "ARQSOFT", "ODSOFT", "QESOFT", "INSIS" , "EINOV", "LABDSOFT", "TMDEI")
                                                  //> mandatoryList  : List[String] = List(TAP, EDOM, ARQSOFT, ODSOFT, QESOFT, INS
                                                  //| IS, EINOV, LABDSOFT, TMDEI)
  val optionalList = List("ESEGI", "SOCOF", "SINFE", "INDES", "SIMOV", "GEPEE")
                                                  //> optionalList  : List[String] = List(ESEGI, SOCOF, SINFE, INDES, SIMOV, GEPEE
                                                  //| )
  
  val courses = mandatoryList :: optionalList :: Nil
                                                  //> courses  : List[List[String]] = List(List(TAP, EDOM, ARQSOFT, ODSOFT, QESOFT
                                                  //| , INSIS, EINOV, LABDSOFT, TMDEI), List(ESEGI, SOCOF, SINFE, INDES, SIMOV, GE
                                                  //| PEE))
  
  
	
	
	
	/*------------- EXERCISE 3 -------------*/
  val list1 =  List.range(9, -1, -1)              //> list1  : List[Int] = List(9, 8, 7, 6, 5, 4, 3, 2, 1, 0)
  //apresenta uma lista entre 9 e -1(exclusive) decrementando os valores de 1 em 1
  
	val list2 = List.range(1, 9, 2)           //> list2  : List[Int] = List(1, 3, 5, 7)
	//apresenta uma lista entre 1 e 9(exclusive) aumentado os valores de 2 em 2
	

	
	
	
	/*------------- EXERCISE 4 -------------*/
	def repeatLists(a:Int, b:Int) =
	{
		List.fill(b)(List.tabulate(a)(n=>n+1))
	}                                         //> repeatLists: (a: Int, b: Int)List[List[Int]]
	
	repeatLists(2,3)                          //> res0: List[List[Int]] = List(List(1, 2), List(1, 2), List(1, 2))
	
	
	
	
	
	/*------------- EXERCISE 5 -------------*/
		def repeatLists2(a:Int, b:Int) =
	{
		List.fill(b)(List.range(1,a+1))
	}                                         //> repeatLists2: (a: Int, b: Int)List[List[Int]]
	
	repeatLists2(2,3)                         //> res1: List[List[Int]] = List(List(1, 2), List(1, 2), List(1, 2))
	
	
	
	
	
	/*------------- EXERCISE 6 -------------*/
	def mySubstring(s: String, i1: Int, i2:Int) = s.substring(i1, i2)
                                                  //> mySubstring: (s: String, i1: Int, i2: Int)String
	mySubstring("TAP is cool",2,5)            //> res2: String = P i
	
	
	
	
	
	/*------------- EXERCISE 7 -------------*/
	def length(xs: List[Int]): Int = {
		
		def lengthAux(acc: Int, ys: List[Int]): Int = ys match {
			case Nil => acc
			case x :: xs => lengthAux(acc+1,ys.tail)
		}
		lengthAux(0,xs)
	}                                         //> length: (xs: List[Int])Int
	
	length(List(1,2,3,4))                     //> res3: Int = 4
	
	
	
	
	/*------------- EXERCISE 8 -------------*/
	def sortStrings_matchcase(xs: List[String]): List[String] = {
		def sortStringsAux(acc:Int, ys: List[String]): List[String] = ys match {
			case Nil => Nil
			case x :: xs => x.sortWith(_ < _) :: sortStringsAux(acc+1, ys.tail)
		}
		sortStringsAux(0,xs)
	}                                         //> sortStrings_matchcase: (xs: List[String])List[String]
	
	sortStrings_matchcase(List("EDOM", "TAP", "ODSOFT"))
                                                  //> res4: List[String] = List(DEMO, APT, DFOOST)
	
	def sortStrings_map(xs: List[String]): List[String] = {
		xs.map(x=> x.sortWith(_ < _))
	}                                         //> sortStrings_map: (xs: List[String])List[String]
	
	sortStrings_map(List("EDOM", "TAP", "ODSOFT"))
                                                  //> res5: List[String] = List(DEMO, APT, DFOOST)
	
	
	
	
	/*------------- EXERCISE 9 -------------*/
  def constructList2(n : Int, f: Int): List[List[Int]] = {
		val list:List[Int]=List(5,2)
		list :: list :: Nil
	}                                         //> constructList2: (n: Int, f: Int)List[List[Int]]
		
	constructList2(5,2)                       //> res6: List[List[Int]] = List(List(5, 2), List(5, 2))
	
	
	
	
	/*------------- EXERCISE 10 -------------*/
	/*igual ao exercicio 8*/
	
	
	
	
	/*------------- EXERCISE 11 -------------*/
	def changePairs(a: List[Int]) = {
		def changePairsAux(b: List[Int]):List[Int] = b match{
			case Nil => Nil
			case x :: a => b.take(2).reverse ::: changePairsAux(b.drop(2))
		}
		changePairsAux(a)
	}                                         //> changePairs: (a: List[Int])List[Int]
	
	changePairs(List(1,2,3,4,5,6,7,8,9,10,11))//> res7: List[Int] = List(2, 1, 4, 3, 6, 5, 8, 7, 10, 9, 11)
	changePairs(List(2,2,30,4,50,6,7,80,9,100))
                                                  //> res8: List[Int] = List(2, 2, 4, 30, 6, 50, 80, 7, 100, 9)
	
	
	
	
	/*------------- EXERCISE 12 -------------*/
	def crazyTransformations1(l: List[String]): List[String] = {
		l.map(s => s.map(c=>c+(c+1).toChar.toString).mkString(""))
	}                                         //> crazyTransformations1: (l: List[String])List[String]
	
	crazyTransformations1(List("EDOM", "TAP", "ODSOFT"))
                                                  //> res9: List[String] = List(EFDEOPMN, TUABPQ, OPDESTOPFGTU)
	
	
	
	/*------------- EXERCISE 13 -------------*/
	def crazyTransformations2(l1: List[String],l2: List[String]): List[String] = {
		val l = l1.head zip l2.head
		List(l.mkString.filterNot("( ) ,".toSet))
	}                                         //> crazyTransformations2: (l1: List[String], l2: List[String])List[String]
	
	crazyTransformations2(List("Helloooo", "MEI"), List("World"))
                                                  //> res10: List[String] = List(HWeolrllod)
	
	crazyTransformations2(List("Helloooo"), List("World", "MEI"))
                                                  //> res11: List[String] = List(HWeolrllod)
	
	
	
	/*------------- EXERCISE 14 -------------*/
	def mySpan(a: List[Int]) : (List[Int],List[Int]) = {
		a.span(x=>x>0)
	}                                         //> mySpan: (a: List[Int])(List[Int], List[Int])
	
	mySpan(List(1, 2, -1, 4,5))               //> res12: (List[Int], List[Int]) = (List(1, 2),List(-1, 4, 5))
	mySpan(List(1, -2, -1, 4,5))              //> res13: (List[Int], List[Int]) = (List(1),List(-2, -1, 4, 5))
	mySpan(List(1, 2, 4,5))                   //> res14: (List[Int], List[Int]) = (List(1, 2, 4, 5),List())
	
	
	
	/*------------- EXERCISE 15 -------------*/
	def applyF(l: List[String], fs: List[String]=> String): String = {
		fs(l)
	}                                         //> applyF: (l: List[String], fs: List[String] => String)String
	
	def f(l: List[String]):String={
		l.foldLeft(new StringBuilder())(_ append _).toString()
	}                                         //> f: (l: List[String])String
	
	applyF(List("T", "A", "P", " is cool!") , f)
                                                  //> res15: String = TAP is cool!
		
		
	
	
	/*------------- EXERCISE 16 -------------*/
	def applyG(l: List[String], fs: List[String]=> String): List[String] = l match {
		
		case Nil => "" :: Nil
		case x::xs=> (fs(l)::applyG(l.tail, fs))
			
	}                                         //> applyG: (l: List[String], fs: List[String] => String)List[String]
	
	def gEx16(l: List[String]):String={
		l.foldLeft(new StringBuilder())(_ append _).toString()
	}                                         //> gEx16: (l: List[String])String
	
	applyG(List("T", "A", "P"," is cool!") , gEx16)
                                                  //> res16: List[String] = List(TAP is cool!, AP is cool!, P is cool!, " is cool
                                                  //| !", "")
	
			
	
	
	/*------------- EXERCISE 17 -------------*/
	 def score(initialList: List[Int], playerList: List[Int], f: Int => Double): Double = {
	  	  
  	val sortedList = initialList.sorted
  	
  	val inBothLists = initialList.intersect(playerList).length
  	val discount = initialList.length - inBothLists

  	
  	 sortedList.foreach{
  		value =>
  		if(value != playerList(sortedList.indexOf(value))){
  			
  			if(sortedList.indexOf(value) == 0){
  				if(discount == 0)
  					return 0
  					
  				return - f(discount)
  				}
  		
  			val index = sortedList.indexOf(value)
  	
				val totalScore =   initialList.length / index
  			val result = ((totalScore.doubleValue() / sortedList.length) - f(discount)).doubleValue()
  			return BigDecimal(result).setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble
  			
  		}
  	}
  	return (1 - f(discount)).doubleValue()
  }                                               //> score: (initialList: List[Int], playerList: List[Int], f: Int => Double)Dou
                                                  //| ble
  	
  
 	def discountVariant ( x: Int ):Double = {
  	x * 0.01
  }                                               //> discountVariant: (x: Int)Double
  
  def noDiscountVariant ( x: Int ):Double = {
  	x * 0.0
  }                                               //> noDiscountVariant: (x: Int)Double
  
  score(List(1, -2, -10, 4,5),List(1, -2, -1, 4,5,10),noDiscountVariant)
                                                  //> res17: Double = -0.0
  score(List(1, -2, -10, 4,5),List(-10, -2,1, 4,5),noDiscountVariant)
                                                  //> res18: Double = 1.0

	
	
	
	}