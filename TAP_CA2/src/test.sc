object test {
		
	  def score(initialList: List[Int], playerList: List[Int], f: Int => Double): Double = {
	  	  
  	val sortedList = initialList.sorted
  	
  	val inBothLists = initialList.intersect(playerList).length
  	val discount = initialList.length - inBothLists

  	
  	 sortedList.foreach{
  		value =>
  		if(value != playerList(sortedList.indexOf(value))){
  			
  			if(sortedList.indexOf(value) == 0){
  				if(discount == 0)
  					return 0
  					
  				return - f(discount)
  				}
  		
  			val index = sortedList.indexOf(value)
  	
				val totalScore =   initialList.length / index
  			val result = ((totalScore.doubleValue() / sortedList.length) - f(discount)).doubleValue()
  			return BigDecimal(result).setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble
  			
  		}
  	}
  	return (1 - f(discount)).doubleValue()
  }                                               //> score: (initialList: List[Int], playerList: List[Int], f: Int => Double)Doub
                                                  //| le
  	
  
 	def discountVariant ( x: Int ):Double = {
  	x * 0.01
  }                                               //> discountVariant: (x: Int)Double
  
  def noDiscountVariant ( x: Int ):Double = {
  	x * 0.0
  }                                               //> noDiscountVariant: (x: Int)Double
  
  score(List(1, -2, -10, 4,5),List(1, -2, -1, 4,5,10),noDiscountVariant)
                                                  //> res0: Double = -0.0
  score(List(1, -2, -10, 4,5),List(-10, -2,1, 4,5),noDiscountVariant)
                                                  //> res1: Double = 1.0

	

}