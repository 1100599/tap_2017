object test {;import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(842); 
		
	  def score(initialList: List[Int], playerList: List[Int], f: Int => Double): Double = {
	  	  
  	val sortedList = initialList.sorted
  	
  	val inBothLists = initialList.intersect(playerList).length
  	val discount = initialList.length - inBothLists

  	
  	 sortedList.foreach{
  		value =>
  		if(value != playerList(sortedList.indexOf(value))){
  			
  			if(sortedList.indexOf(value) == 0){
  				if(discount == 0)
  					return 0
  					
  				return - f(discount)
  				}
  		
  			val index = sortedList.indexOf(value)
  	
				val totalScore =   initialList.length / index
  			val result = ((totalScore.doubleValue() / sortedList.length) - f(discount)).doubleValue()
  			return BigDecimal(result).setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble
  			
  		}
  	}
  	return (1 - f(discount)).doubleValue()
  };System.out.println("""score: (initialList: List[Int], playerList: List[Int], f: Int => Double)Double""");$skip(67); 
  	
  
 	def discountVariant ( x: Int ):Double = {
  	x * 0.01
  };System.out.println("""discountVariant: (x: Int)Double""");$skip(64); 
  
  def noDiscountVariant ( x: Int ):Double = {
  	x * 0.0
  };System.out.println("""noDiscountVariant: (x: Int)Double""");$skip(76); val res$0 = 
  
  score(List(1, -2, -10, 4,5),List(1, -2, -1, 4,5,10),noDiscountVariant);System.out.println("""res0: Double = """ + $show(res$0));$skip(70); val res$1 = 
  score(List(1, -2, -10, 4,5),List(-10, -2,1, 4,5),noDiscountVariant);System.out.println("""res1: Double = """ + $show(res$1))}

	

}
