object ca2_worksheet {;import org.scalaide.worksheet.runtime.library.WorksheetSupport._; def main(args: Array[String])=$execute{;$skip(77); 
	
	//statements:
	val aList:List[Int] = List(1, 2, 3);System.out.println("""aList  : List[Int] = """ + $show(aList ));$skip(41); 
	val bList=List("edom", "odsoft", "tap");System.out.println("""bList  : List[String] = """ + $show(bList ));$skip(26); 
	val cList=List('a', 'b');System.out.println("""cList  : List[Char] = """ + $show(cList ));$skip(29); 
	val dList=List(true, false);System.out.println("""dList  : List[Boolean] = """ + $show(dList ));$skip(11); 
	val e=5.6;System.out.println("""e  : Double = """ + $show(e ));$skip(29); 
	val fList = List(1.0, 2, 3);System.out.println("""fList  : List[Double] = """ + $show(fList ));$skip(11); 
	val g='i';System.out.println("""g  : Char = """ + $show(g ));$skip(92); 
	
	
	
	
	
	/*------------- EXERCISE 1 -------------*/
	val aRes = aList ::: 0.1 :: Nil;System.out.println("""aRes  : List[AnyVal{def getClass(): Class[_ >: Double with Int <: AnyVal]}] = """ + $show(aRes ));$skip(33); 
	val bRes = cList ::: 0.2 :: Nil;System.out.println("""bRes  : List[AnyVal{def getClass(): Class[_ >: Double with Char <: AnyVal]}] = """ + $show(bRes ));$skip(26); 
	val cRes = e :: 5 :: Nil;System.out.println("""cRes  : List[AnyVal] = """ + $show(cRes ));$skip(28); 
	val dRes = e :: 5.1 :: Nil;System.out.println("""dRes  : List[Double] = """ + $show(dRes ));$skip(33); 
	val eRes = aList ::: "4" :: Nil;System.out.println("""eRes  : List[Any] = """ + $show(eRes ));$skip(33); 
	val fRes = fList ::: "4" :: Nil;System.out.println("""fRes  : List[Any] = """ + $show(fRes ));$skip(28); 
	val gRes = g :: "j" :: Nil;System.out.println("""gRes  : List[Any] = """ + $show(gRes ));$skip(171); 
	
	
	
	
	
	/*------------- EXERCISE 2 -------------*/
	val mandatoryList = List("TAP", "EDOM", "ARQSOFT", "ODSOFT", "QESOFT", "INSIS" , "EINOV", "LABDSOFT", "TMDEI");System.out.println("""mandatoryList  : List[String] = """ + $show(mandatoryList ));$skip(80); 
  val optionalList = List("ESEGI", "SOCOF", "SINFE", "INDES", "SIMOV", "GEPEE");System.out.println("""optionalList  : List[String] = """ + $show(optionalList ));$skip(57); 
  
  val courses = mandatoryList :: optionalList :: Nil;System.out.println("""courses  : List[List[String]] = """ + $show(courses ));$skip(98); 
  
  
	
	
	
	/*------------- EXERCISE 3 -------------*/
  val list1 =  List.range(9, -1, -1);System.out.println("""list1  : List[Int] = """ + $show(list1 ));$skip(120); 
  //apresenta uma lista entre 9 e -1(exclusive) decrementando os valores de 1 em 1
  
	val list2 = List.range(1, 9, 2);System.out.println("""list2  : List[Int] = """ + $show(list2 ));$skip(215); 
	//apresenta uma lista entre 1 e 9(exclusive) aumentado os valores de 2 em 2
	

	
	
	
	/*------------- EXERCISE 4 -------------*/
	def repeatLists(a:Int, b:Int) =
	{
		List.fill(b)(List.tabulate(a)(n=>n+1))
	};System.out.println("""repeatLists: (a: Int, b: Int)List[List[Int]]""");$skip(21); val res$0 = 
	
	repeatLists(2,3);System.out.println("""res0: List[List[Int]] = """ + $show(res$0));$skip(134); 
	
	
	
	
	
	/*------------- EXERCISE 5 -------------*/
		def repeatLists2(a:Int, b:Int) =
	{
		List.fill(b)(List.range(1,a+1))
	};System.out.println("""repeatLists2: (a: Int, b: Int)List[List[Int]]""");$skip(22); val res$1 = 
	
	repeatLists2(2,3);System.out.println("""res1: List[List[Int]] = """ + $show(res$1));$skip(126); 
	
	
	
	
	
	/*------------- EXERCISE 6 -------------*/
	def mySubstring(s: String, i1: Int, i2:Int) = s.substring(i1, i2);System.out.println("""mySubstring: (s: String, i1: Int, i2: Int)String""");$skip(32); val res$2 = 
	mySubstring("TAP is cool",2,5);System.out.println("""res2: String = """ + $show(res$2));$skip(246); 
	
	
	
	
	
	/*------------- EXERCISE 7 -------------*/
	def length(xs: List[Int]): Int = {
		
		def lengthAux(acc: Int, ys: List[Int]): Int = ys match {
			case Nil => acc
			case x :: xs => lengthAux(acc+1,ys.tail)
		}
		lengthAux(0,xs)
	};System.out.println("""length: (xs: List[Int])Int""");$skip(26); val res$3 = 
	
	length(List(1,2,3,4));System.out.println("""res3: Int = """ + $show(res$3));$skip(314); 
	
	
	
	
	/*------------- EXERCISE 8 -------------*/
	def sortStrings_matchcase(xs: List[String]): List[String] = {
		def sortStringsAux(acc:Int, ys: List[String]): List[String] = ys match {
			case Nil => Nil
			case x :: xs => x.sortWith(_ < _) :: sortStringsAux(acc+1, ys.tail)
		}
		sortStringsAux(0,xs)
	};System.out.println("""sortStrings_matchcase: (xs: List[String])List[String]""");$skip(57); val res$4 = 
	
	sortStrings_matchcase(List("EDOM", "TAP", "ODSOFT"));System.out.println("""res4: List[String] = """ + $show(res$4));$skip(95); 
	
	def sortStrings_map(xs: List[String]): List[String] = {
		xs.map(x=> x.sortWith(_ < _))
	};System.out.println("""sortStrings_map: (xs: List[String])List[String]""");$skip(51); val res$5 = 
	
	sortStrings_map(List("EDOM", "TAP", "ODSOFT"));System.out.println("""res5: List[String] = """ + $show(res$5));$skip(171); 
	
	
	
	
	/*------------- EXERCISE 9 -------------*/
  def constructList2(n : Int, f: Int): List[List[Int]] = {
		val list:List[Int]=List(5,2)
		list :: list :: Nil
	};System.out.println("""constructList2: (n: Int, f: Int)List[List[Int]]""");$skip(25); val res$6 = 
		
	constructList2(5,2);System.out.println("""res6: List[List[Int]] = """ + $show(res$6));$skip(343); 
	
	
	
	
	/*------------- EXERCISE 10 -------------*/
	/*igual ao exercicio 8*/
	
	
	
	
	/*------------- EXERCISE 11 -------------*/
	def changePairs(a: List[Int]) = {
		def changePairsAux(b: List[Int]):List[Int] = b match{
			case Nil => Nil
			case x :: a => b.take(2).reverse ::: changePairsAux(b.drop(2))
		}
		changePairsAux(a)
	};System.out.println("""changePairs: (a: List[Int])List[Int]""");$skip(47); val res$7 = 
	
	changePairs(List(1,2,3,4,5,6,7,8,9,10,11));System.out.println("""res7: List[Int] = """ + $show(res$7));$skip(45); val res$8 = 
	changePairs(List(2,2,30,4,50,6,7,80,9,100));System.out.println("""res8: List[Int] = """ + $show(res$8));$skip(183); 
	
	
	
	
	/*------------- EXERCISE 12 -------------*/
	def crazyTransformations1(l: List[String]): List[String] = {
		l.map(s => s.map(c=>c+(c+1).toChar.toString).mkString(""))
	};System.out.println("""crazyTransformations1: (l: List[String])List[String]""");$skip(57); val res$9 = 
	
	crazyTransformations1(List("EDOM", "TAP", "ODSOFT"));System.out.println("""res9: List[String] = """ + $show(res$9));$skip(211); 
	
	
	
	/*------------- EXERCISE 13 -------------*/
	def crazyTransformations2(l1: List[String],l2: List[String]): List[String] = {
		val l = l1.head zip l2.head
		List(l.mkString.filterNot("( ) ,".toSet))
	};System.out.println("""crazyTransformations2: (l1: List[String], l2: List[String])List[String]""");$skip(66); val res$10 = 
	
	crazyTransformations2(List("Helloooo", "MEI"), List("World"));System.out.println("""res10: List[String] = """ + $show(res$10));$skip(66); val res$11 = 
	
	crazyTransformations2(List("Helloooo"), List("World", "MEI"));System.out.println("""res11: List[String] = """ + $show(res$11));$skip(128); 
	
	
	
	/*------------- EXERCISE 14 -------------*/
	def mySpan(a: List[Int]) : (List[Int],List[Int]) = {
		a.span(x=>x>0)
	};System.out.println("""mySpan: (a: List[Int])(List[Int], List[Int])""");$skip(32); val res$12 = 
	
	mySpan(List(1, 2, -1, 4,5));System.out.println("""res12: (List[Int], List[Int]) = """ + $show(res$12));$skip(30); val res$13 = 
	mySpan(List(1, -2, -1, 4,5));System.out.println("""res13: (List[Int], List[Int]) = """ + $show(res$13));$skip(25); val res$14 = 
	mySpan(List(1, 2, 4,5));System.out.println("""res14: (List[Int], List[Int]) = """ + $show(res$14));$skip(133); 
	
	
	
	/*------------- EXERCISE 15 -------------*/
	def applyF(l: List[String], fs: List[String]=> String): String = {
		fs(l)
	};System.out.println("""applyF: (l: List[String], fs: List[String] => String)String""");$skip(96); 
	
	def f(l: List[String]):String={
		l.foldLeft(new StringBuilder())(_ append _).toString()
	};System.out.println("""f: (l: List[String])String""");$skip(49); val res$15 = 
	
	applyF(List("T", "A", "P", " is cool!") , f);System.out.println("""res15: String = """ + $show(res$15));$skip(218); 
		
		
	
	
	/*------------- EXERCISE 16 -------------*/
	def applyG(l: List[String], fs: List[String]=> String): List[String] = l match {
		
		case Nil => "" :: Nil
		case x::xs=> (fs(l)::applyG(l.tail, fs))
			
	};System.out.println("""applyG: (l: List[String], fs: List[String] => String)List[String]""");$skip(99); 
	
	def gEx16(l: List[String]):String={
		l.foldLeft(new StringBuilder())(_ append _).toString()
	};System.out.println("""gEx16: (l: List[String])String""");$skip(51); val res$16 = 
	
	applyG(List("T", "A", "P"," is cool!") , gEx16);System.out.println("""res16: List[String] = """ + $show(res$16));$skip(884); 
	
			
	
	
	/*------------- EXERCISE 17 -------------*/
	 def score(initialList: List[Int], playerList: List[Int], f: Int => Double): Double = {
	  	  
  	val sortedList = initialList.sorted
  	
  	val inBothLists = initialList.intersect(playerList).length
  	val discount = initialList.length - inBothLists

  	
  	 sortedList.foreach{
  		value =>
  		if(value != playerList(sortedList.indexOf(value))){
  			
  			if(sortedList.indexOf(value) == 0){
  				if(discount == 0)
  					return 0
  					
  				return - f(discount)
  				}
  		
  			val index = sortedList.indexOf(value)
  	
				val totalScore =   initialList.length / index
  			val result = ((totalScore.doubleValue() / sortedList.length) - f(discount)).doubleValue()
  			return BigDecimal(result).setScale(2, BigDecimal.RoundingMode.HALF_UP).toDouble
  			
  		}
  	}
  	return (1 - f(discount)).doubleValue()
  };System.out.println("""score: (initialList: List[Int], playerList: List[Int], f: Int => Double)Double""");$skip(67); 
  	
  
 	def discountVariant ( x: Int ):Double = {
  	x * 0.01
  };System.out.println("""discountVariant: (x: Int)Double""");$skip(64); 
  
  def noDiscountVariant ( x: Int ):Double = {
  	x * 0.0
  };System.out.println("""noDiscountVariant: (x: Int)Double""");$skip(76); val res$17 = 
  
  score(List(1, -2, -10, 4,5),List(1, -2, -1, 4,5,10),noDiscountVariant);System.out.println("""res17: Double = """ + $show(res$17));$skip(70); val res$18 = 
  score(List(1, -2, -10, 4,5),List(-10, -2,1, 4,5),noDiscountVariant);System.out.println("""res18: Double = """ + $show(res$18))}

	
	
	
	}
