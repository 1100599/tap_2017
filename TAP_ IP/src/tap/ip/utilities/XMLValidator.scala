package tap.ip.utilities

import javax.xml.transform.stream.StreamSource
import javax.xml.validation.SchemaFactory

class XMLValidator(xml: String, xsd: String) {
  private val xmlFile = Constants.filesPath + xml;
  private val xsdFile = Constants.filesPath + xsd

  def validate(): Boolean = {
    try {
      val schemaLang = "http://www.w3.org/2001/XMLSchema"
      val factory = SchemaFactory.newInstance(schemaLang)
      val schema = factory.newSchema(new StreamSource(xsdFile))
      val validator = schema.newValidator()
      validator.validate(new StreamSource(xmlFile))
    } catch {
      case ex: org.xml.sax.SAXParseException => {
        println(ex.getMessage())  
        return false
      }
      case ex: Exception => return false
    }
    true
  }

}