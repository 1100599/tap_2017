package tap.ip.utilities

object Constants {
  
  val filesPath = "Files/";
  
  //Out File
  val outFile = "ip_2017_outFinal.xml";
  
  //XSD Files
  val inXSD = "ip_2017_in.xsd";
  val outXSD = "ip_2017_out.xsd";
  
  //M1 Files
  val inFileM1 = "ip_2017_in.xml";
  val outFilem1 = "IP_Results/out-m1/ip_2017_out_ok_3p_2t.xml";
  
  
  //M2 Files
  val inFileM2v1 = "IP_Results/in/ip_2017_in_ok_3p_2t_v1.xml";
  val inFileM2v2 = "IP_Results/in/ip_2017_in_ok_3p_2t_v2.xml";
  val inFileM2v3 = "IP_Results/in/ip_2017_in_ok_3p_2t_v3.xml";
  val inFileM2v4 = "IP_Results/in/ip_2017_in_ok_3p_2t_v4.xml";
  val inFileM2v5 = "IP_Results/in/ip_2017_in_ok_3p_2t_v5.xml"; 

  val outFileM2v1 = "IP_Results/out-m2/ip_2017_out_ok_3p_2t_v1.xml";
  val outFileM2v2 = "IP_Results/out-m2/ip_2017_out_ok_3p_2t_v2.xml";
  val outFileM2v3 = "IP_Results/out-m2/ip_2017_out_ok_3p_2t_v3.xml";
  val outFileM2v4 = "IP_Results/out-m2/ip_2017_out_ok_3p_2t_v4.xml";
  val outFileM2v5 = "IP_Results/out-m2/ip_2017_out_ok_3p_2t_v5.xml";
  
  //M3 Files
  val inFileM3v1 = "m3_tests/t1/ip_2017_in_ok_2o_3p_2t_v1.xml";
  val inFileM3v2 = "m3_tests/t2/ip_2017_in_ok_2o_3p_2t_v2.xml";
  val inFileM3v3 = "m3_tests/t3/ip_2017_in_ok_2o_3p_2t_v3.xml";
  val inFileM3v4 = "m3_tests/t4/ip_2017_in_ok_2o_3p_2t_v4.xml";
  val inFileM3v5 = "m3_tests/t5/ip_2017_in_ok_2o_3p_2t_v5.xml";

  val outFileM3v1 = "m3_tests/t1/ip_2017_out_ok_2o_3p_2t_v1.xml";
  val outFileM3v2 = "m3_tests/t2/ip_2017_out_ok_2o_3p_2t_v2.xml";
  val outFileM3v3 = "m3_tests/t3/ip_2017_out_ok_2o_3p_2t_v3.xml";
  val outFileM3v4 = "m3_tests/t4/ip_2017_out_ok_2o_3p_2t_v4.xml";
  val outFileM3v5 = "m3_tests/t5/ip_2017_out_ok_2o_3p_2t_v5.xml";
  
  



}