package tap.ip.utilities

import scala.xml._;
import tap.ip.model._;
import tap.ip.bl._
import tap.ip.model.PhysicalResources
import tap.ip.model.Tasks
import tap.ip.model.Orders
import tap.ip.model.HumanResources
import tap.ip.model.Products

class XParser(filename: String) extends XMLParser {
  private val file = XML.loadFile(Constants.filesPath + filename);

  def parseProduction(): Production = {
    new Production(parsePhysicalResources(), parseTasks(), parseHumanResources(), parseProducts(), parseOrders());
  }

  def parseSchedule(): Schedule = {
    new Schedule(parseTaskSchedules());
  }

  def saveSchedule(fileToSave: String, xml: Node) = {
    scala.xml.XML.save(Constants.filesPath + fileToSave, xml, "UTF-8");
    println("Out file saved in: " + Constants.filesPath + fileToSave);
  }

  private def parsePhysicalResources(): PhysicalResources = {
    val list = (file \ "PhysicalResources" \\ "Physical").toList;

    def parse(ln: List[Node]): List[Physical] = ln match {
      case Nil     => Nil
      case x :: xs => new Physical(x.attribute("id").get.toString(), x.attribute("type").get.toString()) :: parse(xs)
    }

    new PhysicalResources(parse(list));
  }

  private def parseTasks(): Tasks = {
    val list = (file \ "Tasks" \\ "Task").toList;

    def parseTaskPhysicalResources(node: Node): List[String] = {
      val list = (node \ "PhysicalResource").toList;

      def parse(ln: List[Node]): List[String] = ln match {
        case Nil     => Nil
        case x :: xs => x.attribute("prstype").get.toString() :: parse(xs)
      }

      parse(list);
    }

    def parse(ln: List[Node]): List[Task] = ln match {
      case Nil     => Nil
      case x :: xs => new Task(x.attribute("id").get.toString(), x.attribute("time").get.toString().toInt, parseTaskPhysicalResources(x)) :: parse(xs)
    }

    new Tasks(parse(list));
  }

  private def parseHumanResources(): HumanResources = {
    val list = (file \ "HumanResources" \\ "Human").toList;

    def parseHandles(node: Node): List[Handles] = {
      val list = (node \ "Handles").toList;

      def parse(ln: List[Node]): List[Handles] = ln match {
        case Nil     => Nil
        case x :: xs => new Handles(x.attribute("type").get.toString()) :: parse(xs)
      }

      parse(list);
    }

    def parse(ln: List[Node]): List[Human] = ln match {
      case Nil     => Nil
      case x :: xs => new Human(x.attribute("id").get.toString(), x.attribute("name").get.toString(), parseHandles(x)) :: parse(xs)
    }

    new HumanResources(parse(list));
  }

  private def parseProducts(): Products = {
    val list = (file \ "Products" \\ "Product").toList;

    def parseProcesses(node: Node): List[Process] = {
      val list = (node \ "Process").toList;

      def parse(ln: List[Node]): List[Process] = ln match {
        case Nil     => Nil
        case x :: xs => new Process(x.attribute("tskref").get.toString()) :: parse(xs)
      }

      parse(list);
    }

    def parse(ln: List[Node]): List[Product] = ln match {
      case Nil     => Nil
      case x :: xs => new Product(x.attribute("id").get.toString(), x.attribute("name").get.toString(), parseProcesses(x)) :: parse(xs)
    }

    new Products(parse(list));
  }

  private def parseOrders(): Orders = {
    val list = (file \ "Orders" \\ "Order").toList;

    def parse(ln: List[Node]): List[Order] = ln match {
      case Nil     => Nil
      case x :: xs => new Order(x.attribute("id").get.toString(), x.attribute("prdref").get.toString(), x.attribute("quantity").get.toString().toInt) :: parse(xs)
    }
    new Orders(parse(list));
  };

  private def parseTaskSchedules(): List[TaskSchedule] = {
    val list = (file \ "TaskSchedule").toList;

    def parseTaskPhysicalResources(node: Node): PhysicalResources = {
      val list = (node \ "PhysicalResources" \\ "Physical").toList;

      def parse(ln: List[Node]): List[Physical] = ln match {
        case Nil     => Nil
        case x :: xs => new Physical(x.attribute("id").get.toString(), "") :: parse(xs)
      }

      new PhysicalResources(parse(list));
    }

    def parseTaskHumanResources(node: Node): HumanResources = {
      val list = (node \ "HumanResources" \\ "Human").toList;

      def parse(ln: List[Node]): List[Human] = ln match {
        case Nil     => Nil
        case x :: xs => new Human("", x.attribute("name").get.toString(), List()) :: parse(xs)
      }

      new HumanResources(parse(list));
    }

    def parse(ln: List[Node]): List[TaskSchedule] = ln match {
      case Nil => Nil
      case x :: xs => new TaskSchedule(
        x.attribute("order").get.toString(),
        x.attribute("productNumber").get.toString().toInt,
        x.attribute("task").get.toString(),
        x.attribute("start").get.toString().toInt,
        x.attribute("end").get.toString().toInt,
        parseTaskPhysicalResources(x),
        parseTaskHumanResources(x)) :: parse(xs)
    }

    parse(list);
  };
}