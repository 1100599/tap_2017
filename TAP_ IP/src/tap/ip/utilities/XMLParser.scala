package tap.ip.utilities

import tap.ip.bl._
import scala.xml._;
import tap.ip.model._;

trait XMLParser {
   
  def parseProduction(): Production;
  
  def parseSchedule(): Schedule;

  def saveSchedule(fileToSave:String, xml:Node);
}