package tap.ip.bl

import tap.ip.model._

class Factory(physicalResources: PhysicalResources, tasks: Tasks, humans: HumanResources, products: Products) {

  def getSchedule(orders: Orders): Schedule = {

    val humansPhysicals = MapHumansOnPhysical(this.humans.list, this.physicalResources.list)

    val programTasks = getTaskProgram(orders.list)

    val allLists = programTasks.permutations.toList

    def getAllSchedule(l: List[List[TaskProgram]]): List[List[TaskSchedule]] = l match {
      case Nil => Nil
      case x :: xs => {
        getSchedule(x, 0, this.physicalResources, this.humans, Nil, Nil) :: getAllSchedule(xs)
      }
    }

    val lists = getAllSchedule(allLists)

    val bestChoise = lists.sortBy(f => f.sortBy(o => o.end).head.end).head

    return new Schedule(bestChoise);
  }

  private def getHumanForPhysicals(p: List[Physical], h: List[Human]): List[Human] = p match {
    case Nil => Nil
    case x :: xs => {
      val hAuxList = h.find(p => p.handlesList.exists(u => u.hType == x.pType))

      if (!hAuxList.isEmpty) {
        val hAux = hAuxList.get
        hAux :: getHumanForPhysicals(xs, h.filter(h => h.id != hAux.id))
      } else
        getHumanForPhysicals(xs, h)
    }
  }

  private def MapHumansOnPhysical(h: List[Human], p: List[Physical]): List[HumanPhysicalResource] = h match {
    case Nil => Nil
    case x :: xs => {

      HandlePhysical(x.handlesList, p, x) ::: MapHumansOnPhysical(xs, p)
    }
  }

  private def GetResourcesForTask(task: Task, hp: List[HumanPhysicalResource]): List[HumanPhysicalResource] = {

    hp.filter(f => task.physicalResources.contains(f.physical.pType))

  }

  private def GetResourcesDistinctForTask(task: List[String], hp: List[HumanPhysicalResource], hpInUse: List[HumanPhysicalResource]): List[HumanPhysicalResource] = task match {
    case Nil => Nil
    case xh :: xhList => {
      val op = hp.find(v => v.physical.pType == xh && hpInUse.exists(e => e.physical.id == v.physical.id && e.human.id == v.human.id) == false)

      if (op.isEmpty) {
        GetResourcesDistinctForTask(xhList, hp, hpInUse)
      } else {

        val res = op.get

        if (hpInUse.exists(u => u.human.id == res.human.id && u.physical.id == res.physical.id))
          GetResourcesDistinctForTask(xhList, hp, hpInUse)
        else
          res :: GetResourcesDistinctForTask(xhList, hp, res :: hpInUse)

      }
    }

  }

  private def getProductById(id: String): Product = {
    return products.list.find(x => x.id == id).get
  }

  private def getTasksForProduct(product: Product): List[Task] = {

    def getTasks(process: List[Process]): List[Task] = process match {
      case Nil     => Nil
      case x :: xs => getTaskById(x.tskref) :: getTasks(xs)
    }
    return getTasks(product.processList)
  }
  private def getHumansListForTask(task: Task): List[Human] = {

    def getHumans(p: List[Physical]): List[Human] = p match {
      case Nil     => Nil
      case x :: xs => getHumanListForPhysicalResource(x.pType) ::: getHumans(xs)
    }
    val pList = getPhysicalResourceListForTask(task)

    return getHumans(pList)
  }

  private def getHumanListForPhysicalResource(pType: String): List[Human] = {

    humans.list.filter(x => x.handlesList.exists(y => y.hType == pType))

  }

  private def getPhysicalResourceListForTask(task: Task): List[Physical] = {

    def getPhysical(p: List[String]): List[Physical] = p match {
      case Nil     => Nil
      case x :: xs => getPhysicalResourceListIdYask(x) ::: getPhysical(xs)
    }
    return getPhysical(task.physicalResources)

  }

  private def getPhysicalResourceListIdYask(pType: String): List[Physical] = {

    physicalResources.list.filter(x => x.pType == pType)

  }

  private def getTaskById(id: String): Task = {
    tasks.list.find(x => x.id == id).get
  }

  private def programTasks(l: List[Task], pNumber: Int, order: Order, product: Product): List[TaskProgram] = l match {
    case Nil => Nil;
    case x :: xs =>
      {
        val aux = List.range(0, order.quantity)
        productN(aux, order, product, x, 1) ::: programTasks(xs, pNumber, order, product)
      }
  }

  private def getTaskProgram(orders: List[Order]): List[TaskProgram] = orders match {
    case Nil => Nil
    case x :: xs => {

      val aux = List.range(0, x.quantity)
      val product = getProductById(x.prdref)
      val tasks = getTasksForProduct(product)

      programTasks(tasks, 0, x, product) ::: getTaskProgram(xs)
    }
  }

  private def getSchedule(l: List[TaskProgram], time: Int, p: PhysicalResources, h: HumanResources, tasksToComplete: List[TaskSchedule], tasksInWait: List[TaskProgram]): List[TaskSchedule] = l match {
    case Nil => Nil
    case x :: xs => {

      if (tasksToComplete.exists(t => t.productNumber == x.productNumber && t.task == x.task.id && t.order == x.order))
        return getSchedule(xs, time, p, h, tasksToComplete, tasksInWait)
        
       val freeResources = getResourcesToRelease(tasksToComplete, time)  

      if (tasksToComplete.exists(t => t.productNumber == x.productNumber && t.task != x.task.id && t.order == x.order && t.end > time)) {

        if ((x :: xs).length <= tasksInWait.length) {

          val stilForComplete = tasksToComplete.filter(k => k.end > time)

          val d = tasksToComplete.sortBy(f => f.end).head

          return getSchedule(xs :+ x, time + 1, new PhysicalResources(p.list ::: freeResources._2), new HumanResources(h.list ::: freeResources._1), stilForComplete, tasksInWait)
        } else {

          val stilForComplete = tasksToComplete.filter(k => k.end > time)

          val d = tasksToComplete.sortBy(f => f.end).head

          return getSchedule(xs :+ x, time, new PhysicalResources(p.list ::: freeResources._2), new HumanResources(h.list ::: freeResources._1), stilForComplete, x :: tasksInWait)
        }

      }

      val task = x.task

      val humansPhysicalsAux = MapHumansOnPhysical(h.list, p.list)
      val humansPhysicals = GetResourcesForTask(task, humansPhysicalsAux)

      val humansAux = humansPhysicals.filter(v => h.list.exists(k => k.id == v.human.id))
      val physicalsAux = humansPhysicals.filter(v => humansAux.exists(k => k.physical.id == v.physical.id))

      val humans = physicalsAux.map(f => f.human)
      val physicals = physicalsAux.map(f => f.physical)

      val distAux = GetResourcesDistinctForTask(task.physicalResources, physicalsAux, Nil)

      if (humans == null || physicals == null || humans.length == 0 || physicals.length == 0 || distAux.length < task.physicalResources.length) {
        
        val stilForComplete = tasksToComplete.filter(k => k.end > time)

        if (tasksToComplete != null && tasksToComplete.length > 0) {
          val d = tasksToComplete.sortBy(f => f.end).head
          return getSchedule(xs :+ x, time + (d.end - time), new PhysicalResources(p.list ::: freeResources._2), new HumanResources(h.list ::: freeResources._1), stilForComplete, tasksInWait)
        } else {
          return getSchedule(xs :+ x, time, new PhysicalResources(p.list ::: freeResources._2), new HumanResources(h.list ::: freeResources._1), stilForComplete, tasksInWait)

        }
      } else {

        val te = physicalsAux.sortBy(f => f.human.handlesList.length)

        val dist = GetResourcesDistinctForTask(task.physicalResources, te, Nil)

        val taskS = new TaskSchedule(x.order, x.productNumber, task.id, time, time + task.time, new PhysicalResources(dist.map(f => f.physical)), new HumanResources(dist.map(f => f.human)))
                
        val stilForComplete = tasksToComplete.filter(k => k.end > time)

        val freeHumans = new HumanResources(h.list.filter(hu => taskS.humanResources.list.exists(o => o.id == hu.id) == false) ::: freeResources._1)
        val freePhysicals = new PhysicalResources(p.list.filter(ph => taskS.physicalResources.list.exists(r => ph.id == r.id) == false) ::: freeResources._2)

        return taskS :: getSchedule(xs, time, freePhysicals, freeHumans, taskS :: stilForComplete, tasksInWait)
      }
    }
  }

  private def HandlePhysical(handles: List[Handles], p: List[Physical], h: Human): List[HumanPhysicalResource] = handles match {
    case Nil => Nil
    case xh :: xhList => {
      val phan = p.find(pp => pp.pType == xh.hType)
      if (phan.isEmpty) {
        HandlePhysical(xhList, p, h)
      } else
        new HumanPhysicalResource(h, phan.get) :: HandlePhysical(xhList, p, h)
    }
  }

  private def productN(l: List[Int], order: Order, product: Product, task: Task, pNumber: Int): List[TaskProgram] = l match {
    case Nil => Nil;
    case x :: xs =>
      {

        val taskHumans = getHumansListForTask(task)
        val pList = getPhysicalResourceListForTask(task)

        val program = new TaskProgram(order.id, pNumber, task, new PhysicalResources(pList), new HumanResources(taskHumans))

        program :: productN(xs, order, product, task, pNumber + 1)

      }
  }

  private def factorial(n: Int): Int = {
    if (n == 0)
      return 1
    else
      return n * factorial(n - 1)
  }

  private def getResourcesToRelease(tasksToComplete: List[TaskSchedule], time: Int): (List[Human], List[Physical]) = {

    val tasksCompleted = tasksToComplete.filter(o => o.end <= time)
    val humansToRelease = tasksCompleted.map(f => f.humanResources.list).flatten
    val physicalToRelease = tasksCompleted.map(f => f.physicalResources.list).flatten

    return (humansToRelease, physicalToRelease);
  }

}