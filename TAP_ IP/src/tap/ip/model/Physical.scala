package tap.ip.model

class Physical(val id: String, val pType: String) {

  def toXML =
    {
      <Physical id={ id } />      
    }

}