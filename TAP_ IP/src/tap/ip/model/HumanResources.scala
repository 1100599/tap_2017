package tap.ip.model

class HumanResources(val list: List[Human]) {

  def toXML =
    {
      <HumanResources>
        { list map (e => e.toXML) }
      </HumanResources>
    }

}