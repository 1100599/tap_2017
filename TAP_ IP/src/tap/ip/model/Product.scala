package tap.ip.model

class Product(val id: String, val name: String, val processList: List[Process]) {

  def toXML =
    {
      <Product id={ id } name={ name }>
        { processList map (e => e.toXML) }
      </Product>
    }

}