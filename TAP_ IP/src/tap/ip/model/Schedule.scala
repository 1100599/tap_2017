package tap.ip.model

class Schedule(val taskSchedules: List[TaskSchedule]) {

  def toXML =
    {
      <Schedule xmlns="http://www.dei.isep.ipp.pt/ip_2017" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.dei.isep.ipp.pt/ip_2017 ip_2017_out.xsd ">
        { taskSchedules map (ts => ts.toXml) }
      </Schedule>
    }

}