package tap.ip.model

class Orders(val list: List[Order]) {

  def toXML =
    {
      <Orders>
        { list map (e => e.toXML) }
      </Orders>
    }

}