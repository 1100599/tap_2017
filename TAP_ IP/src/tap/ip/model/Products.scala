package tap.ip.model

class Products(val list: List[Product]) {

  def toXML =
    {
      <Products>
        { list map (e => e.toXML) }
      </Products>
    }

}