package tap.ip.model

class PhysicalResources(val list: List[Physical]) {

  def toXML =
    {
      <PhysicalResources>
        { list map (e => e.toXML) }
      </PhysicalResources>
    }

}