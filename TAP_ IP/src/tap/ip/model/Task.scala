package tap.ip.model

class Task(val id: String, val time: Int, val physicalResources: List[String]) {

  def toXML =
    {
      <Task id={ id } time={ time.toString() }>
      </Task>
    }

}