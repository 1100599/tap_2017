package tap.ip.model

import scala.xml._

class TaskSchedule(val order: String, val productNumber: Int, val task: String, val start: Int, val end: Int, val physicalResources: PhysicalResources, val humanResources: HumanResources) {
  
  def toXml = {
    <TaskSchedule order={order} productNumber={productNumber.toString()}  task={task}  start={start.toString()}  end={end.toString()}>
      {physicalResources.toXML}
      {humanResources.toXML}
    </TaskSchedule>
  }
 
}


class TaskProgram(val order: String, val productNumber: Int, val task: Task, val physicalResources: PhysicalResources, val humanResources: HumanResources)