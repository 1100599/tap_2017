package tap.ip.model

class Handles(val hType: String) {

  def toXML =
    {
      <Handles type={hType}/>
    }

}