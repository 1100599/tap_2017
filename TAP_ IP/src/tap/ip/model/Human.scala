package tap.ip.model

class Human(val id: String, name: String, val handlesList: List[Handles]) {

  def toXML =
    {
      <Human name={ name } />
    }

}