package tap.ip.model

class Production(val physicalResources: PhysicalResources, val tasks: Tasks, val humanResources: HumanResources, val products: Products, val orders: Orders) {

  def toXML =
    {
      <Production xmlns="http://www.dei.isep.ipp.pt/ip_2017" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.dei.isep.ipp.pt/ip_2017 ip_2017_in.xsd ">
        { physicalResources.toXML }
        { tasks.toXML }
        { humanResources.toXML }
        { products.toXML }
        { orders.toXML }
      </Production>
    }

}