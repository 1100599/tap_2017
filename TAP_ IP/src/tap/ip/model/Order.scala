package tap.ip.model

class Order(val id: String, val prdref: String, val quantity: Int) {

  def toXML =
    {
      <Order id={ id } prdref={ prdref } quantity={ quantity.toString() }/>
    }

}