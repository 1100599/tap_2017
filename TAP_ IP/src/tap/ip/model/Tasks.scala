package tap.ip.model

class Tasks(val list: List[Task]) {

  def toXML =
    {
      <Tasks>
        { list map (e => e.toXML) }
      </Tasks>
    }

}