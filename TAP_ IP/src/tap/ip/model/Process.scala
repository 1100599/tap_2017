package tap.ip.model

class Process(val tskref: String) {

  def toXML =
    {
      <Process tskref={ tskref }/>
    }

}