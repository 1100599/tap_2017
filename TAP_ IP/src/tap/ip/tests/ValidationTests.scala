package tap.ip.tests

import java.util.Calendar
import org.scalatest.FunSuite
import tap.ip.utilities._
import tap.ip.model._
import tap.ip.bl._;

class ValidationTests extends FunSuite {

  test("Test 1 - Check if in file is valid") {
    val inXMLValidator = new XMLValidator(Constants.inFileM1, Constants.inXSD);
    assert(inXMLValidator.validate());
  }

  test("Test 2 - Check if out file is valid") {
    val outXMLValidator = new XMLValidator(Constants.outFile, Constants.outXSD);
    assert(outXMLValidator.validate());
  }

}