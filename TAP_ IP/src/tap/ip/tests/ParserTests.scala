package tap.ip.tests

import java.util.Calendar
import org.scalatest.FunSuite
import tap.ip.utilities._
import tap.ip.model._
import tap.ip.bl._;

class ParserTests extends FunSuite {

  test("Test 1 - Check if Physical Resources are created as expected") {

    val xmlParser = new XParser(Constants.inFileM1);
    val production = xmlParser.parseProduction();
    val physicalResourcesExpected = List(
      new Physical("PRS_1", "PRST 1"),
      new Physical("PRS_2", "PRST 1"),
      new Physical("PRS_3", "PRST 2"));

    val listXML = production.physicalResources.list map (pr => pr.id);
    val listExpected = physicalResourcesExpected map (pr => pr.id);

    assert(listXML == listExpected);

  }

  test("Test 2 - Check if m1 XML File is created as expected") {

    val xmlParser = new XParser(Constants.inFileM1);
    val production = xmlParser.parseProduction();
    val factory = new Factory(production.physicalResources, production.tasks, production.humanResources, production.products);
    val schedule = factory.getSchedule(production.orders);
    xmlParser.saveSchedule(Constants.outFile, schedule.toXML);

    val xmlParserFinalOut = new XParser(Constants.outFile);
    val finalOutSchedule = xmlParserFinalOut.parseSchedule();

    val xmlParserExpectedOut = new XParser(Constants.outFile);
    val expectedOutSchedule = xmlParserExpectedOut.parseSchedule();

    val listFromXML = finalOutSchedule.taskSchedules map (ts => ts.toXml);
    val listExpected = expectedOutSchedule.taskSchedules map (ts => ts.toXml);
    assert(listFromXML == listExpected);

  }

  test("Test 3 - Check if Task Schedule are in output as expected") {

    val xmlParser = new XParser(Constants.outFileM2v1);

    val schedule = xmlParser.parseSchedule();

    val TaskSchedulesExpected: List[TaskSchedule] = List(
      new TaskSchedule("ORD_1", 1, "TSK_1", 0, 10,
        new PhysicalResources(List(new Physical("PRS_1", ""))),
        new HumanResources(List(new Human("", "Antonio", List())))),
      new TaskSchedule("ORD_1", 1, "TSK_2", 10, 25,
        new PhysicalResources(List(new Physical("PRS_3", ""))),
        new HumanResources(List(new Human("", "Jose", List())))),
      new TaskSchedule("ORD_1", 2, "TSK_1", 10, 20,
        new PhysicalResources(List(new Physical("PRS_1", ""))),
        new HumanResources(List(new Human("", "Antonio", List())))),
      new TaskSchedule("ORD_1", 2, "TSK_2", 25, 40,
        new PhysicalResources(List(new Physical("PRS_3", ""))),
        new HumanResources(List(new Human("", "Jose", List())))),
      new TaskSchedule("ORD_1", 3, "TSK_1", 25, 35,
        new PhysicalResources(List(new Physical("PRS_1", ""))),
        new HumanResources(List(new Human("", "Antonio", List())))),
      new TaskSchedule("ORD_1", 3, "TSK_2", 40, 55,
        new PhysicalResources(List(new Physical("PRS_3", ""))),
        new HumanResources(List(new Human("", "Jose", List())))));

    val listFromXML = schedule.taskSchedules map (ts => ts.toXml);
    val listExpected = TaskSchedulesExpected map (ts => ts.toXml);
    assert(listFromXML == listExpected);

  }
  
   test("Test 4 - Check if M2v1 XML File is created as expected") {

    val xmlParser = new XParser(Constants.inFileM2v1);
    val production = xmlParser.parseProduction();
    val factory = new Factory(production.physicalResources, production.tasks, production.humanResources, production.products);
    val schedule = factory.getSchedule(production.orders);
    xmlParser.saveSchedule(Constants.outFile, schedule.toXML);

    val xmlParserFinalOut = new XParser(Constants.outFile);
    val finalOutSchedule = xmlParserFinalOut.parseSchedule();

    val xmlParserExpectedOut = new XParser(Constants.outFileM2v1);
    val expectedOutSchedule = xmlParserExpectedOut.parseSchedule();

    val listFromXML = finalOutSchedule.taskSchedules map (ts => ts.toXml);
    val listExpected = expectedOutSchedule.taskSchedules map (ts => ts.toXml);
    assert(listFromXML == listExpected);

  }

  test("Test 5 - Check if v2 XML File is created as expected") {

    val xmlParser = new XParser(Constants.inFileM2v2);
    val production = xmlParser.parseProduction();
    val factory = new Factory(production.physicalResources, production.tasks, production.humanResources, production.products);
    val schedule = factory.getSchedule(production.orders);
    xmlParser.saveSchedule(Constants.outFile, schedule.toXML);

    val xmlParserFinalOut = new XParser(Constants.outFile);
    val finalOutSchedule = xmlParserFinalOut.parseSchedule();

    val xmlParserExpectedOut = new XParser(Constants.outFileM2v1);
    val expectedOutSchedule = xmlParserExpectedOut.parseSchedule();

    val listFromXML = finalOutSchedule.taskSchedules map (ts => ts.toXml);
    val listExpected = expectedOutSchedule.taskSchedules map (ts => ts.toXml);
    assert(listFromXML == listExpected);

  }

  test("Test 6 - Check if v3 XML File is created as expected") {

    val xmlParser = new XParser(Constants.inFileM2v3);
    val production = xmlParser.parseProduction();
    val factory = new Factory(production.physicalResources, production.tasks, production.humanResources, production.products);
    val schedule = factory.getSchedule(production.orders);
    xmlParser.saveSchedule(Constants.outFile, schedule.toXML);

    val xmlParserFinalOut = new XParser(Constants.outFile);
    val finalOutSchedule = xmlParserFinalOut.parseSchedule();

    val xmlParserExpectedOut = new XParser(Constants.outFileM2v3);
    val expectedOutSchedule = xmlParserExpectedOut.parseSchedule();

    val listFromXML = finalOutSchedule.taskSchedules map (ts => ts.toXml);
    val listExpected = expectedOutSchedule.taskSchedules map (ts => ts.toXml);
    

    assert(listFromXML == listExpected);

  }

}