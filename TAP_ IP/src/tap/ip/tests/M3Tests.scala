package tap.ip.tests

import java.util.Calendar
import org.scalatest.FunSuite
import tap.ip.utilities._
import tap.ip.model._
import tap.ip.bl._;
import scala.xml._;

class M3Tests extends FunSuite {

  def getListFromXML(inFile: String): List[Elem] = {
    val xmlParser = new XParser(inFile);
    val production = xmlParser.parseProduction();
    val factory = new Factory(production.physicalResources, production.tasks, production.humanResources, production.products);
    val schedule = factory.getSchedule(production.orders);
    xmlParser.saveSchedule(Constants.outFile, schedule.toXML);

    val xmlParserFinalOut = new XParser(Constants.outFile);
    val finalOutSchedule = xmlParserFinalOut.parseSchedule();

    finalOutSchedule.taskSchedules map (ts => ts.toXml);
  }

  def getListExpected(outFile: String): List[Elem] = {
    val xmlParserExpectedOut = new XParser(outFile);
    val expectedOutSchedule = xmlParserExpectedOut.parseSchedule();

    expectedOutSchedule.taskSchedules map (ts => ts.toXml);
  }

  test("Test 1 - Check if M3v1 XML File is created as expected") {
    val listFromXML = getListFromXML(Constants.inFileM3v1);
    val listExpected = getListExpected(Constants.outFileM3v1);

    try {
      assert(listFromXML == listExpected, "Everything is as expected");
    } catch {
      case ex: Exception => assert(false, "Something inside one of the TaskSchedules isn't as expected");
    }
  }

  test("Test 2 - Check if M3v2 XML File is created as expected") {
    val listFromXML = getListFromXML(Constants.inFileM3v2);
    val listExpected = getListExpected(Constants.outFileM3v2);

    try {
      assert(listFromXML == listExpected, "Everything is as expected");
    } catch {
      case ex: Exception => assert(false, "Something inside one of the TaskSchedules isn't as expected");
    }
  }

  test("Test 3 - Check if M3v3 XML File is created as expected") {
    val listFromXML = getListFromXML(Constants.inFileM3v3);
    val listExpected = getListExpected(Constants.outFileM3v3);

    try {
      assert(listFromXML == listExpected, "Everything is as expected");
    } catch {
      case ex: Exception => assert(false, "Something inside one of the TaskSchedules isn't as expected");
    }
  }

  test("Test 4 - Check if M3v4 XML File is created as expected") {
    val listFromXML = getListFromXML(Constants.inFileM3v4);
    val listExpected = getListExpected(Constants.outFileM3v4);
   
    try {
      assert(listFromXML == listExpected, "Everything is as expected");
    } catch {
      case ex: Exception => assert(false, "Something inside one of the TaskSchedules isn't as expected");
    }
  }

  test("Test 5 - Check if M3v5 XML File is created as expected") {
    val listFromXML = getListFromXML(Constants.inFileM3v5);
    val listExpected = getListExpected(Constants.outFileM3v5);
    
    try {
      assert(listFromXML == listExpected, "Everything is as expected");
    } catch {
      case ex: Exception => assert(false, "Something inside one of the TaskSchedules isn't as expected");
    }
  }
}