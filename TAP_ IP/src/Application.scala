import tap.ip.utilities._
import tap.ip.bl._

object Application extends App {
  println("INTEGRATIVE PROJECT - TAP\n");

  //check if inFile exists
  val file = new java.io.File(Constants.filesPath + Constants.inFileM1);
  if (!file.exists()) {
    println("Error: " + Constants.filesPath + Constants.inFileM1 + " does not exists!");
  } else {
    //validate inFile with xsd
    val inXMLValidator = new XMLValidator(Constants.inFileM1, Constants.inXSD);

    if (inXMLValidator.validate()) {
      println("Success: inFile XML - VALID!\n");

      val xmlParser = new XParser(Constants.inFileM1);
      
      val production = xmlParser.parseProduction();
      
      val factory = new Factory(production.physicalResources, production.tasks, production.humanResources, production.products);

      val schedule = factory.getSchedule(production.orders);

      xmlParser.saveSchedule(Constants.outFile, schedule.toXML);

      val outXMLValidator = new XMLValidator(Constants.outFile, Constants.outXSD);
      if (outXMLValidator.validate()) {
        println("Success: outFile XML - VALID!\n");
      } else {
        println("\nError: outFile XML - INVALID!");
      }

    } else {
      println("\nError: inFile XML - INVALID!");
    };
  }
}