package isep.tap.ca3.crosscutting.log

import isep.tap.ca3.logger.TimeTagLogger
import java.util.Calendar

/*
 * TODO: create the LogAdapter object
 */

package object LogAdapter {  
  
  
  implicit class LogAdapter(val timeLog:TimeTagLogger) extends Log
  {
    override def log(msg:String):Unit = {
      
       val tStamp = Calendar.getInstance().getTime().toString()
       
       timeLog.log(msg, tStamp) 
    }    
    
    override def top():(String,String) = {
       timeLog.top() 
    }
    
  } 
}