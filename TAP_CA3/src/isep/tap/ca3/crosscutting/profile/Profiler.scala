package isep.tap.ca3.crosscutting.profile

import isep.tap.ca3.dataReader.DataReader
import isep.tap.ca3.dataReader.RecipeList
import isep.tap.ca3.dataReader.DataReader


/*
 * TODO: create the Profiler trait, which forces the reader to return a ProfilerList
 */

trait Profiler extends DataReader{
  

    
  override def readDataFast(fname: String): ProfilerList[Int]  = {
    time{
      read(fname)
      }    
  }

  override def readDataSlow(fname: String): ProfilerList[Int] = {
    time { 
      (1 to 10000).foreach {
      case num =>
        read(fname)
      }
    read(fname)
    }
  }
  
  def time(block: => RecipeList):  ProfilerList[Int] = {
    val t0 = System.nanoTime()
    val result = block    // call-by-name
    val t1 = System.nanoTime()
    val t = (t1 - t0).toInt
    
    new ProfilerList[Int ](result.lr,t)
    
}
  
  

}
