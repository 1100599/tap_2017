package isep.tap.ca3.logger

import scala.collection.mutable.Stack

final class TimeTagLogger {
  var s = new Stack[(String,String)]
  
  def log(message: String, tStamp: String): Unit = {
    s.push( (message, tStamp) )
  }
  
  def top(): (String, String) = {
    s.top
  }  
}