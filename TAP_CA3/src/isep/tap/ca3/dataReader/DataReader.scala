package isep.tap.ca3.dataReader

import isep.tap.ca3.model.Recipe

trait DataReader {
  
  def read(fname: String): RecipeList  

  def readDataFast(fname: String): RecipeList =
    read(fname)

  def readDataSlow(fname: String): RecipeList = {
    (1 to 10000).foreach {
      case num =>
        read(fname)
    }
    read(fname)
  }
}