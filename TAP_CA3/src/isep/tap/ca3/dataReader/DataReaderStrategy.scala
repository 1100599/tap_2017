package isep.tap.ca3.dataReader
import isep.tap.ca3.model.Recipe

object DataReaderStrategy {
  
  def apply(filename: String): Option[ (String) => RecipeList ] =
    filename match {
      case f if f.endsWith(".json") => Some(parseJson)
      case f if f.endsWith(".csv") => Some(parseCsv)
      case f => None
    }

  
  def parseJson(file: String): RecipeList = {
    val json = new DataReaderImplJSON
    json.read(file)
  }
  
  def parseCsv(file: String): RecipeList = {
    val csv = new DataReaderImplCSV
    csv.read(file)
  }
  
}