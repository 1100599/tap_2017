package isep.tap.ca3.dataReader

import com.github.tototoshi.csv._
import scala.io.Source
import isep.tap.ca3.model.Recipe


class DataReaderImplCSV extends DataReader {
  
  override def read(fname: String): RecipeList = {
    val pwd:String = System.getProperty("user.dir")+"/files/"+fname
    val file = CSVReader.open(pwd)
    val ret = file.all().tail.map(r=>Recipe(r.head,r.last))
    file.close()
    new RecipeList(ret)
  }
  
}