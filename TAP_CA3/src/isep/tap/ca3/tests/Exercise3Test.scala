package isep.tap.ca3.tests

import org.scalatest.FunSuite
import isep.tap.ca3.model.Recipe
import isep.tap.ca3.dataReader._

class Exercise3Test extends FunSuite {

  /* TODO: uncomment the test when DataReaderImplCSV is created */

  test("Exercise 3 initial conditions...") {
    val dataReader = new DataReaderImplCSV

    val expected = List(Recipe("Portuguese Green Soup", "Portugal"), Recipe("Grilled Sardines", "Portugal"), Recipe("Salted Cod with Cream", "Portugal"))
    val other_expected = List(Recipe("German Sauerkraut", "Germany"), Recipe("Risotto", "Italy"), Recipe("Tortilla", "Spain"))

    assert(dataReader.readDataFast("recipe.csv").lr === expected)
    assert(dataReader.readDataSlow("other_recipe.csv").lr === other_expected)
  }

  /* TODO: create the tests */

  // strategy does not exist
  test("Exercise 3. Test 1.") {
    val txtStrategy = DataReaderStrategy("recipe.txt")

    assert(txtStrategy === None)
  }

  // read one file JSON and another csv, with same content, verify equality
  test("Exercise 3. Test 2.") {
    val csvStrategy = DataReaderStrategy("recipe.csv")
    val jsonStrategy = DataReaderStrategy("recipe.json")

    //check if Strategies exists
    assert(csvStrategy !== None)
    assert(jsonStrategy !== None)

    val csvResult = csvStrategy.get("recipe.csv")
    val jsonResult = jsonStrategy.get("recipe.json")

    //check if result is the same
    assert(csvResult.lr === jsonResult.lr)
  }

  // read one file JSON and another csv, with different content, verify inequality
  test("Exercise 3. Test 3.") {
    val csvStrategy = DataReaderStrategy("recipe.csv")
    val jsonStrategy = DataReaderStrategy("other_recipe.json")

    //check if Strategies exists
    assert(csvStrategy !== None)
    assert(jsonStrategy !== None)

    val csvResult = csvStrategy.get("recipe.csv")
    val jsonResult = jsonStrategy.get("other_recipe.json")

    //check if result is not the same
    assert(csvResult.lr !== jsonResult.lr)

  }
}