package isep.tap.ca3.tests

import org.scalatest.FunSuite
import java.util.Calendar
import isep.tap.ca3.crosscutting.log.LogAdapter._
import isep.tap.ca3.crosscutting.log.Log
import isep.tap.ca3.logger.TimeTagLogger

class Exercise2Test extends FunSuite{
  
  test("Exercise 2. SimpleLogger") {
      
    val log: Log = new TimeTagLogger
    
    val msg = "Dumy message!"
    val tt = Calendar.getInstance().getTime().toString() // Current date/time
    
    log.log(msg)
    assert(log.top()===(msg,tt))
    
    Thread.sleep(1000)
    
    log.log(msg)
    assert(log.top()!==(msg,tt))
          
  }   
}